#  Controller Functions

This file contains samples of controller functions used in various applications showing conding standard and code usage.

## Disclaimer
Code fragments may not be complete, this is done to prevent sensitive/security related information to be leaked.

### Sample 1

Code sample fetching profile value of authenticated user for editing

```php
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        $user->loadMissing([
            'profile', 'address', 'services', 'areas', 'cities'
        ]);

        $services = Service::all();
        $cities = City::all();
        $areas = CityArea::all();

        return view('account.profile', compact('user', 'cities', 'areas', 'services'));
    }
```


### Sample 2
Code sample demonstrating reject action in a controller with dependency injection of form request. The response of the function is an json response using transformer 

```php
    public function reject(RejectCaseRequest $request)
    {
        $case = $request->retrieve('case') ?? Cases::findOrFail($request->id);
        $case = $this->call(
            RejectCaseAction::class,
            [$case, Cases::VERIFICATION_REJECTED, implode(PHP_EOL, $request->reasons)]
        );

        return $this->transform($case, CasesTransformer::class);
    }

```


### Sample 3

Sample demonstrates storing of content in the application with flash messages. The function checks correct permission before proceding

```php
    /**
     * @param StoreCommentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCommentRequest $request)
    {

        $case = Cases::findOrFail($request->case_id);
        
        //check only validate user can perform the operation
        if ($request->has('comment_type') && $request->comment_type == 'rejected') {
            if (!auth()->user()->hasPermissionTo('case-submission-approved')
                && in_array($case->type, [Cases::PENDING_APPROVAL])
            ) {
                abort(404);
            }
            $this->call(RejectCaseAction::class, [$case, Cases::SUBMISSION_REJECTED, $request->text]);
        } else {
            Apiato::call('Comment@CreateCommentAction', [$request]);
        }

        return redirect()->back()->with('success', 'Comment added successfully.');
    }
```