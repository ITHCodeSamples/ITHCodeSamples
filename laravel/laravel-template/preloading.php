<?php

$files = [
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/create_uploaded_file.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/marshal_headers_from_sapi.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/marshal_method_from_sapi.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/marshal_protocol_version_from_sapi.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/marshal_uri_from_sapi.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/normalize_server.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/normalize_uploaded_files.php",
    "/var/www/vendor/zendframework/zend-diactoros/src/functions/parse_cookie_header.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Contracts/Encryption/Encrypter.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Encryption/Encrypter.php",
    "/var/www/vendor/barryvdh/laravel-cors/src/HandleCors.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Contracts/Routing/UrlGenerator.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Support/Facades/Route.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/RouteRegistrar.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/RouteGroup.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/Route.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/RouteDependencyResolverTrait.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/RouteAction.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/ResourceRegistrar.php",
    "/var/www/vendor/laravel/framework/src/Illuminate/Routing/PendingResourceRegistration.php",
    "/var/www/vendor/symfony/routing/Route.php",
    "/var/www/vendor/symfony/routing/RouteCompiler.php",
    "/var/www/vendor/symfony/routing/RouteCompilerInterface.php",
    "/var/www/vendor/symfony/routing/CompiledRoute.php"
];

foreach ($files as $file) {
    opcache_compile_file($file);
}
