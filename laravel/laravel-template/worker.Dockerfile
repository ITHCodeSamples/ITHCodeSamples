# Build Compoer Dependencies
FROM php:7.4-cli-alpine AS build-composer

ARG GITHUB_PERSONAL_TOKEN=""

COPY ./ /var/www


# Installing extensions
RUN docker-php-ext-install opcache && docker-php-ext-enable opcache && docker-php-ext-install pdo mysqli pdo_mysql && docker-php-ext-enable pdo pdo_mysql mysqli

# Installing composer dependencies
WORKDIR /var/www
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php && php -r "unlink('composer-setup.php');" \
    && php composer.phar config --global github-oauth.github.com $GITHUB_PERSONAL_TOKEN \
    && php composer.phar install --no-dev && rm composer.phar

# Settings up php ini settings and  opacache settings
COPY preloading.php /preloading.php
COPY php.ini /usr/local/etc/php/php.ini

# Creating user and group for supervisor
RUN addgroup -g 1000 supervisor && \
    adduser -D -G supervisor -u 1000 supervisor

# Installing supervisor and setting persmissions
RUN apk update && apk add supervisor && chmod +x /preloading.php \
    && chmod -R 775 /var/www/storage

# Copying supervisor configurations
COPY supervisor.conf /etc/supervisord.conf
COPY supervisor-laravel-worker.conf /etc/supervisord.d/supervisor-laravel-worker.conf

WORKDIR /var/www
CMD supervisord -n -c /etc/supervisord.conf
