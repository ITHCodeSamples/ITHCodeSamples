<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Resources\Json\JsonResource;

abstract class AbstractResourceStatus extends JsonResource
{
    /**
     * @var null
     */
    public static $wrap = null;

    /**
     * The default status code
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Sets the status code
     *
     * @param int $code
     * @return $this
     */
    public function setStatusCode(int $code)
    {
        $this->statusCode = $code;
        return $this;
    }

    /**
     * Changes response structure
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\JsonResponse $response
     */
    public function withResponse($request, $response)
    {
        parent::withResponse($request, $response);
        $response->setStatusCode($this->statusCode);
    }
}
