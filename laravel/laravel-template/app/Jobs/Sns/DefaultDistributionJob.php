<?php

namespace App\Jobs\Sns;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class DefaultDistributionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The topic being used
     *
     * @var string
     */
    public $topic;

    /**
     * The topic data
     *
     * @var array
     */
    public $data;

    /**
     * The environment suffix
     *
     * @var \Illuminate\Config\Repository|mixed
     */
    public $topicSuffix;

    public const MAPPED_TOPICS = [
        'topic-name' => DefaultDistributionJob::class, // replace it with correct job name,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $topic, array $data)
    {
        $this->topic = $topic;
        $this->data = $data;
        $this->topicSuffix = config('queue.connections.sns-sqs.sns-config.suffix');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws
     */
    public function handle()
    {
        $job = $this->mappedJob();
        dispatch(new $job($this->topic, $this->data));
    }

    /**
     * Returns correct job
     *
     * @return string
     * @throws Exception
     */
    public function mappedJob(): string
    {
        $topic = Str::replaceArray($this->topicSuffix, [''], $this->topic);

        if (!Arr::exists(self::MAPPED_TOPICS, $topic)) {
            throw new Exception('Unmapped topic ' . $topic);
        }

        return self::MAPPED_TOPICS[$topic];
    }
}
