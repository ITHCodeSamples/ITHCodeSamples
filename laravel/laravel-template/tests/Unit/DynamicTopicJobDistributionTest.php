<?php

namespace Tests\Unit;

use App\Jobs\Sns\DefaultDistributionJob;
use Tests\TestCase;

class DynamicTopicJobDistributionTest extends TestCase
{
    /**
     * Test to check if mapping function returns correct job if topic is mapped
     */
    public function testMappedJobsReturningCorrectJob()
    {
        $jobDistribution = new DefaultDistributionJob(
            'topic-name' . config('queue.connections.sns-sqs.sns-config.suffix'),
            []
        );
        $this->assertEquals(DefaultDistributionJob::class, $jobDistribution->mappedJob());
    }

    /**
     * Test to check if mapping function throws error when no job is found
     */
    public function testMappingJobThrowsErrorIfNoJobIsFound()
    {
        $topic = 'non-topic-name';
        $this->expectExceptionMessage('Unmapped topic ' . $topic);

        $jobDistribution = new DefaultDistributionJob(
            $topic . config('queue.connections.sns-sqs.sns-config.suffix'),
            []
        );

        $jobDistribution->mappedJob();
    }
}
