<?php

return [
    'config' => [
        'region' => env('AWS_DEFAULT_REGION'),
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'suffix' => env('TOPIC_SUFFIX', '-dev'),
        'arn-prefix' => env('TOPIC_ARN_PREFIX')
    ],
    'topics' => [
        'example' => 'topic name'
    ]
];
