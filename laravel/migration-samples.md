# Migration Samples
Contains sample codes how a migration file within laravel is created and maintained.

## Disclaimer
Code fragments may not be complete, this is done to prevent sensitive/security related information to be leaked.

### Sample 1

The sample demonstrates how an table create migration works with nullable and non nullable columns and a foreign key. 
```php
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('first_name');
            $table->string('last_name');
            $table->text('address_line1')->nullable()->default(NULL);
            $table->text('address_line2')->nullable()->default(NULL);
            $table->integer('country_id')->unsigned()->nullable()->default(NULL);
            $table->integer('state_id')->unsigned()->nullable()->default(NULL);
            $table->string('city')->nullable()->default(NULL);
            $table->string('zip_code')->nullable()->default(NULL);
            $table->string('company_name')->nullable()->default(NULL);
            $table->string('profile_image')->nullable()->default(NULL);
            $table->string('phone_number')->nullable()->defaul(NULL);

            $table->timestamps();

        });
    }
```

### Sample 2

The sample demonstrates a migration code for linking a comments table to multiple tables with foreign keys.

```php
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('case_id');
            $table->foreign('case_id')->references('id')->on('cases');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('text');
            $table->timestamps();

        });
    }
```


### Sample 3
The sample demonstrates how a existing table is updated with a new column.

```php
    Schema::table('photos', function (Blueprint $table) {
        $table->dateTime('archived_on')->nullable()->default(NULL);
    });
```