#  Drupal Functions and Sample Code

This file contains samples of Drupal custom and action/hooks functions used in various applications showing conding standard and code usage.

## Disclaimer
Code fragments may not be complete, this is done to prevent sensitive/security related information to be leaked.

### Sample 1

Override the breadcrumb for a specific page via view

```php
    function theme_name_views_pre_render(&$view) {
      if ($view->name == 'course_detail' && $view->current_display == 'page') {
        // Add help text to the user login block.
       $breadcrumbs = drupal_get_breadcrumb();
       $breadcrumbs[] = '<a href="/course-list">Knowledge Paths</a>';
       
        $npath = explode('/', drupal_get_normal_path($view->args[0]));
        $node = node_load($npath[1]);
        $breadcrumbs[] = t($node->title);
    
        drupal_set_breadcrumb($breadcrumbs);
      }
    
      
    }

```


### Sample 2
Implement hook to create template for per content type

```php
   function theme_name_preprocess_page(&$vars) {
     if (isset($vars['node']->type)) {
       $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
     }
   }

```


### Sample 3

//implementation of hook_menu_alter()

```php
   function theme_name_custom_menu_alter(&$items) {
       $items['user/%user/view']['title'] = "About Me";
       //delete the feed delete button from admin area
       unset($items['import/%feeds_importer/delete-items']);
   }
```


### Sample 4

Adding new menu item to admin menu using custom code

```php
    function theme_name_custom_menu()
    {      $items['user/%user/my_content'] = array(
            'title' => 'My Content',
            'page callback' => 'my_content_blank_page',
            'access callback' => 'user_is_logged_in', //only users who login
            'weight' => -90,
            'type' => MENU_LOCAL_TASK,
        );    
            
           return $items;
    }
```


### Sample 5

Hide reset password link

```php
    function ani_hide_reset_pass_menu_alter(&$items) {
        $items['user/login']['type'] = MENU_CALLBACK;
        $items['user/register']['type'] = MENU_CALLBACK;
        $items['user/password']['type'] = MENU_CALLBACK;
    }
```


### Sample 6

include diffrent type of files in a custom module

```php
    drupal_add_css(drupal_get_path('module', 'course_status') . '/css/course_status.css', array('weight'=>999,'group' => CSS_THEME, 'every_page' => false));
            //include inc file responsible for all db query
            module_load_include('inc', 'course_status', 'includes/course_status');
            
    //generating pass variables from php to js
    drupal_add_js(array('course_status_ani' => array('basePath' => drupal_get_path('module', 'course_status_ani'),'courseId'=>$course_id,'contentId'=>$content_id, 'logged_in' =>$logged_in)), 'setting');      

```

### Sample 7

database related

```php
    
        global $user;
            $data = array();
            //getting course is already marked or not
            $query = db_select('course_status', 'cs');
            $query->condition('cs.media_nid', $contentId);
            $query->condition('cs.course_nid', $courseId);
            $query->condition('cs.uid', $user->uid);
            $query->addField('cs', 'id');
            $num_rows = $query->countQuery()->execute()->fetchField();
            $result = $query->execute();
            $res = $result->fetchField();
        
            // The transaction opens here.
            $txn = db_transaction();
            try{
                //if already marked
                if($num_rows > 0)
                {
                    $data['timestamp'] = REQUEST_TIME;
                    $data['status'] = $status;
        
                    $num_updated = db_update('course_status')
                        ->fields($data)
                        ->condition('id',$res)
                        ->execute();
        
                }
                else{
                    $data['course_nid'] = $courseId;
                    $data['media_nid'] = $contentId;
                    $data['uid'] = $user->uid;
                    $data['timestamp'] = REQUEST_TIME;
                    $data['status'] = $status;
        
                    $id = db_insert('course_status')
                        ->fields($data)
                        ->execute();
                }
                if($status==1)
                {
                    $output =  array('action'=>1,'status'=>1,'msg'=>'You have marked this item complete.');
                }else{
                    $output =  array('action'=>0,'status'=>1,'msg'=>'You have marked this item incomplete.');
                }
        
            }
            //if transaction failed
            catch(Exception $e){
                // Something went wrong somewhere, so roll back now.
                $txn->rollback();
                // Log the exception to watchdog.
               // watchdog_exception('type', $e);
                $output = array('status'=>0,'msg'=>'Update failed');
            }
            return drupal_json_encode($output);
          // return $output;

```