const purple = '#78315b';
const blue = '#003248';

export default {
  red: {
    backgroundColor: purple,
    borderColor: purple,
    pointBorderColor: purple,
  },
  blue: {
    backgroundColor: blue,
    borderColor: blue,
    pointBorderColor: blue,
  }
};
