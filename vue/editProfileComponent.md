# Edit profile component

* The code has been taken from a Laravel 5.7 project.
* This is a complete example for user edit profile.
* CSS is based on bootstrap v4

### profile.blade.php
```blade
@extends('_layouts.app')
@section('pageTitle','Profile')

@section('content')
  @component('components.breadcrumb')
      Profile
    @endcomponent
  <div class="container mid-section view-profile pt-0">
    <edit-profile :profile='@json($user->profile)'></edit-profile>   
  </div>
@endsection

```

### EditProfile.vue
```html
<template>
  <form method="post" enctype="multipart/form-data" @submit.prevent="submit">
    <section class="card-white wrap-form">
      <div class="img-round round-big">
        <img :src="preview" alt="avatar">
      </div>
      <div class="form-group">
        <label>Upload Photo</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input form-control" id="avatar-file" @change="onFileChange"
                 accept="image/*" :class="{ 'is-invalid': errors.has('avatar') }">
          <label class="custom-file-label" for="avatar-file"></label>
          <form-error :bag="errors" field="avatar"></form-error>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>First Name</label>
            <input type="text" v-model="form.first_name" class="form-control"
                   :class="{ 'is-invalid': errors.has('first_name') }">
            <form-error :bag="errors" field="first_name"></form-error>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" v-model="form.last_name" class="form-control"
                   :class="{ 'is-invalid': errors.has('last_name') }">
            <form-error :bag="errors" field="last_name"></form-error>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Bio</label>
        <textarea rows="5" v-model="form.bio" class="form-control"
                  :class="{ 'is-invalid': errors.has('bio') }"></textarea>
        <form-error :bag="errors" field="bio"></form-error>
      </div>
      <div class="form-group">
        <label>Experience</label>
        <textarea rows="5" v-model="form.experience" class="form-control"
                  :class="{ 'is-invalid': errors.has('experience') }"></textarea>
        <form-error :bag="errors" field="experience"></form-error>
      </div>
      <div class="form-group">
        <label>Education</label>
        <textarea rows="5" v-model="form.education" class="form-control"
                  :class="{ 'is-invalid': errors.has('education') }"></textarea>
        <form-error :bag="errors" field="education"></form-error>
      </div>
      <div class="form-group text-right">
        <input type="submit" value="Save" class="btn btn-purple" :disabled="processing">
      </div>
    </section>
  </form>
</template>

<script>
  import formErrorsMixin from '@/mixins/formErrors';

  import {objectToFormData} from 'form-backend-validation/dist/util/formData';

  export default {
    name: 'edit-profile',
    mixins: [formErrorsMixin],
    props: {
      profile: {
        type: Object,
        required: true
      },
    },
    data() {
      return {
        preview: this.profile.avatar_thumb_url,
        processing: false,
        form: this.profile,
      }
    },
    methods: {
      submit() {
        this.processing = true;

        this.$http.post(route('account.profile.updateProfile'), this.getPayload())
          .then(response => {
            this.errors.clear();
            this.preview = response.data.avatar_thumb_url;
            // todo show notification message
          })
          .catch(error => {
            this.recordErrors(error)
          })
          .then(() => {
            // Reset file
            this.form.avatar = null;
            this.processing = false;
          })
      },
      getPayload() {
        return objectToFormData(this.form)
      },
      onFileChange(event) {
        let files = event.target.files || event.dataTransfer.files;

        if (!files.length) return;

        if (!files[0].type.match('image.*')) {
          this.form.avatar = null;
          event.target.value = null;
          return;
        }

        this.form.avatar = files[0];

        let reader = new FileReader();

        reader.onload = (e) => {
          this.preview = e.target.result;
        };

        reader.readAsDataURL(files[0]);
      }
    }
  }
</script>

```

### components/formError.vue
```html
<!-- formError component to work with form-backend-validation package -->
<template>
  <div class="invalid-feedback" v-show="hasError">
    {{error}}
  </div>
</template>

<script>
  import {Errors} from 'form-backend-validation';

  export default {
    name: 'form-error',
    props: {
      bag: {
        type: Errors,
        required: true,
      },
      field: {
        type: String,
        required: true
      }
    },
    computed: {
      hasError() {
        return this.bag.has(this.field)
      },
      error() {
        return this.bag.first(this.field)
      }
    }
  }
</script>
```

### mixins/formErrors.js
```js
import {Errors} from 'form-backend-validation';

export default {
  data() {
    return {
      errors: new Errors(),
    }
  },
  methods: {
    recordErrors(error) {
      if (error && error.response && error.response.data.errors) {
        this.errors.record(error.response.data.errors);
      }
    }
  }
}

```
