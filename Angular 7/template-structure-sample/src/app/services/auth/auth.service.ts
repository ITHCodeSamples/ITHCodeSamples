import { Injectable } from '@angular/core';
import { TransferState } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { ACCESS_TOKEN } from '../server/state-keys';
import { AuthState } from '../server/auth/auth-state';
import { Token } from './token';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements AuthState {

  protected token: Token | null = null;

  constructor(public stateService: TransferState) {
    this.init();
  }

  /**
   * @inheritDoc
   */
  public init() {
    if (environment.token.override) {
      this.token = {access_token: environment.token.token} as Token;
    } else {
      this.token = this.stateService.get<Token>(ACCESS_TOKEN, null);
    }
    console.log('token value is', this.token, 'from web');
  }

  /**
   * @inheritDoc
   */
  getToken(): string {
    return this.token === null ? '' : this.token.access_token;
  }

  /**
   * @inheritDoc
   */
  refreshToken(): Observable<Token> {
    // @todo
    return of({} as Token);
  }
}
