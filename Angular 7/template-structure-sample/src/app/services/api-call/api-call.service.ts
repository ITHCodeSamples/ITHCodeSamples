import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(
    public http: HttpClient
  ) { }

  /**
   * Returns main url mapped with base url
   * @param url
   */
  public apiUrl(url: string): string {
    return `${environment.api.url}/${url}`;
  }
}
