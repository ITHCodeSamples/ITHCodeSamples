import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageState } from '../server/language/language-state';
import { LanguageService as ServerLanguageService } from '../server/language/language.service';
import { TransferState } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class LanguageService extends ServerLanguageService implements LanguageState {

  constructor(
    public stateService: TransferState,
    public translate: TranslateService
  ) {
    super(stateService, translate);
  }

  /**
   * Initializes the language
   */
  public init() {
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }

  /**
   * Sets active language
   * @param language
   */
  public setActiveLang(language: string) {
    this.activeLang = language;
    this.translate.use(this.activeLang);
  }
}
