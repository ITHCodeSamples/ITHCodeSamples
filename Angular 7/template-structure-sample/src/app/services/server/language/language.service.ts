import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { LanguageState } from './language-state';
import { dynamicKey, LANGUAGE_VALUES } from '../state-keys';

@Injectable({
  providedIn: 'root'
})
export class LanguageService implements LanguageState {

  protected defaultLang = 'en';

  protected activeLang = 'en';

  constructor(
    public stateService: TransferState,
    public translate: TranslateService
  ) {
  }

  /**
   * Initializes language
   */
  public init() {
    this.translate.addLangs([this.defaultLang]);
    this.translate.setDefaultLang(this.defaultLang);
    this.translate.use(this.activeLang);
    this.translate.getTranslation(this.activeLang).subscribe(langKeys => {
      this.stateService.set(dynamicKey(`languageValue:${this.activeLang}`), langKeys);
    });
  }

  /**
   * Sets active language
   * @param language
   */
  public setActiveLang(language: string) {
    this.activeLang = language;
    this.translate.use(this.activeLang);
    this.translate.getTranslation(this.activeLang).subscribe(langKeys => {
      this.stateService.set(dynamicKey(`languageValue:${language}`), langKeys);
    });
  }

  /**
   * Returns currently active language
   */
  public getActiveLang(): string {
    return this.activeLang;
  }

  /**
   * Add languages to the list
   * @param languages
   */
  public addLanguages(languages: string[]) {
    this.translate.addLangs(languages);
  }
}
