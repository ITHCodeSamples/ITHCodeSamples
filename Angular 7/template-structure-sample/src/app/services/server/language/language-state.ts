export interface LanguageState {
  init();

  setActiveLang(language: string);

  getActiveLang(): string;

  addLanguages(languages: string[]);
}
