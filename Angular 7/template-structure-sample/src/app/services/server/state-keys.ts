import { makeStateKey, StateKey } from '@angular/platform-browser';
import { Token } from '../auth/token';

export const LANGUAGE_VALUES = [makeStateKey<string>('languageValues')];
export const ACCESS_TOKEN = makeStateKey<Token>('accessToken');

export function dynamicKey(key: string): StateKey<string> {
  return makeStateKey<string>(key);
}


