import { Inject, Injectable } from '@angular/core';
import { AuthState } from './auth-state';
import { Observable, of } from 'rxjs';
import { Token } from '../../auth/token';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { Request } from 'express';
import { TransferState } from '@angular/platform-browser';
import { ACCESS_TOKEN } from '../state-keys';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements AuthState {

  protected token: Token | null = null;

  constructor(
    public stateService: TransferState,
    @Inject(REQUEST) protected request: Request
  ) {
    this.stateService.set<null>(ACCESS_TOKEN, null);
  }

  /**
   * @inheritDoc
   */
  getToken(): string {
    return this.token === null ? '' : this.token.access_token;
  }

  /**
   * @inheritDoc
   */
  init() {
    const request: any = this.request;
    if (request.session.tokenInfo) {
      this.token = request.session.tokenInfo;
      this.stateService.set<Token>(ACCESS_TOKEN, this.token);
    }
    console.log('token value is', this.token, 'from server');
  }

  /**
   * @inheritDoc
   */
  refreshToken(): Observable<Token> {
    // @todo
    return of({} as Token);
  }
}
