import { Observable } from 'rxjs';
import { Token } from '../../auth/token';

export interface AuthState {

  /**
   * Initialize the state
   */
  init();

  /**
   * Returns the current token
   */
  getToken(): string;

  /**
   * Refreshes the current token information using refresh token
   */
  refreshToken(): Observable<Token>;
}
