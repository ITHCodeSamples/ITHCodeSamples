import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor/token-interceptor.service';

export const interceptors: Provider[] = [
  {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}
];
