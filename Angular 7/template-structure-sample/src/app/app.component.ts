import { Component } from '@angular/core';
import { LanguageService } from './services/language/language.service';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'profile-application';

  constructor(protected language: LanguageService, auth: AuthService) {
    auth.init();
    this.language.init();
  }
}
