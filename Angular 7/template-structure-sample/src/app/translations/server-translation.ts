import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import * as fs from 'fs';
import * as path from 'path';


export class ServerTranslation extends TranslateLoader {
  constructor() {
    super();
  }

  getTranslation(lang: string): Observable<object> {
    return new Observable<object>((observer) => {
      fs.readFile(path.join(__dirname, `../browser/assets/i18n/${lang}.json`), 'utf8', (error, data) => {
        if (error) {
          observer.error('Unable to find file');
          return;
        }
        observer.next(JSON.parse(data));
        observer.complete();
      });
    });
  }
}
