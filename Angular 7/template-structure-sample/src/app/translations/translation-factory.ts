// AoT requires an exported function for factories
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TransferState } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { dynamicKey } from '../services/server/state-keys';


class CustomTranslateHttpLoader extends TranslateHttpLoader {
  constructor(http: HttpClient, protected state: TransferState) {
    super(http);
  }

  public getTranslation(lang: string): Observable<object> {
    if (this.state.hasKey(dynamicKey(`languageValue:${lang}`))) {
      return of(this.state.get(dynamicKey(`languageValue:${lang}`), super.getTranslation(lang)));
    }
    return super.getTranslation(lang);
  }
}


export function HttpLoaderFactory(http: HttpClient, state: TransferState) {
  return new CustomTranslateHttpLoader(http, state);
}
