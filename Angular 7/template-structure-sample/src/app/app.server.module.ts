import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ServerTranslation } from './translations/server-translation';
import { LanguageService } from './services/language/language.service';
import { LanguageService as ServerLanguageService } from './services/server/language/language.service';
import { AuthService } from './services/auth/auth.service';
import { AuthService as ServerAuthService } from './services/server/auth/auth.service';
import { interceptors } from './interceptors/interceptors';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
    HttpClientModule,
    ServerTransferStateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: ServerTranslation
      }
    })
  ],
  providers: interceptors.concat([
    {provide: LanguageService, useClass: ServerLanguageService},
    {provide: AuthService, useClass: ServerAuthService}
  ]),
  bootstrap: [AppComponent],
})
export class AppServerModule {}
