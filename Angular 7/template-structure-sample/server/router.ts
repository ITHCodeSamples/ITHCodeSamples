import { Router } from 'express';
import { authRouter } from './router-actions/authentication';

export const router = Router();
router.use('/auth', authRouter);

