import { SessionInterface } from '../interfaces/configurations';
import { environment } from '../environment/env';

export const sessionConfig: SessionInterface = {
  secret: environment.sessionKey,
  saveUninitialized: false,
  driver: environment.sessionDriver,
  redis: environment.redis
};
