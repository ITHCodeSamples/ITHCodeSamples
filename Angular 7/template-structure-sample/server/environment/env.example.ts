interface Environment {
  clientId: string;
  clientSecret: string;
  authServiceUrl: string;
  redirectUrl: string;
  sessionKey: string;
  sessionDriver: 'local' | 'redis';
  redis?: {
    port: number;
    host: string;
  };
}


export const environment: Environment = {
  clientId: '1',
  clientSecret: 'secret',
  authServiceUrl: 'https://authentication.fast.com',
  redirectUrl: 'http://localhost:4000',
  sessionKey: 'Random Value',
  sessionDriver: 'local'
};
