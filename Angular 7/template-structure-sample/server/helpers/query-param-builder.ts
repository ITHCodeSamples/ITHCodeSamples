export interface QueryParamBuilder {
  name: string;
  value: string;
}

/**
 * Creates query parameter string for all params
 * @param params
 */
export const queryBuilder = (params: QueryParamBuilder[]): string => {
  return params.reduce((carry, param, index) => {
    const mappingItem = index === 0 ? '' : '&';
    return `${carry}${mappingItem}${param.name}=${param.value}`;
  }, '');
};
