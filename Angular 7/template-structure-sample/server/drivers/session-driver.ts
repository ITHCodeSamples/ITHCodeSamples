import { SessionInterface } from '../interfaces/configurations';
import { SessionOptions } from 'express-session';

export class SessionDriver {

  constructor(protected config: SessionInterface) {
  }

  /**
   * Returns session option configuration
   *
   * @param expressSessionFunc
   */
  public session(expressSessionFunc): SessionOptions {
    switch (this.config.driver) {
      case 'local':
        return {
          secret: this.config.secret,
          saveUninitialized: this.config.saveUninitialized
        };

      case 'redis':
        return this.redisStore(expressSessionFunc);

      default:
        throw new Error(`No driver found for ${this.config.driver}`);
    }

  }

  /**
   * Returns redis client
   * @param expressSessionFunc
   */
  protected redisStore(expressSessionFunc): SessionOptions {
    const RedisStore = require('connect-redis')(expressSessionFunc);
    const redisClient = require('redis').createClient(this.config.redis.port, this.config.redis.host);

    return {
      store: new RedisStore(redisClient),
      secret: this.config.secret,
      saveUninitialized: this.config.saveUninitialized
    };
  }

}
