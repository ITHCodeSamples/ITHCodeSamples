import { Request, Response } from 'express';
import { queryBuilder } from '../helpers/query-param-builder';
import { environment } from '../environment/env';
import * as Axios from 'axios';
import { AxiosError, AxiosResponse } from 'axios';
import { Token } from '../interfaces/token';
import { Router } from 'express';

/**
 * Request handler for login route
 * @param req
 * @param res
 */
const login = (req: Request, res: Response) => {
  const state = Math.random();
  const query = queryBuilder([
    {name: 'client_id', value: environment.clientId},
    {name: 'redirect_uri', value: environment.redirectUrl},
    {name: 'response_type', value: 'code'},
    {name: 'state', value: state.toString()}
  ]);
  req.session.state = state;
  res.redirect(`${environment.authServiceUrl}/oauth/authorize?${query}`);
};

/**
 * Request handler for redirection route
 *
 * @param req
 * @param res
 */
const redirectHandler = (req: Request, res: Response) => {

  if (req.session.state.toString() !== req.query.state.toString()) {
    res.redirect('/', 200);
  }

  Axios.default.post(`${environment.authServiceUrl}/oauth/token`, {
    grant_type: 'authorization_code',
    client_id: environment.clientId,
    client_secret: environment.clientSecret,
    redirect_uri: environment.redirectUrl,
    code: req.query.code,
  }, {
    headers: {
      Accept: 'application/json',
    }
  }).then((result: AxiosResponse<Token>) => {
    req.session.tokenInfo = result.data;
    res.redirect('/');
  }).catch(e => {
    console.error('something went wrong', e);
  });
};

/**
 * Refresh the access token using the refresh token.
 * @param req
 * @param res
 */
const refreshToken = (req: Request, res: Response) => {
  Axios.default.post(`${environment.authServiceUrl}/oauth/token`, {
    grant_type: 'refresh_token',
    client_id: environment.clientId,
    client_secret: environment.clientSecret,
    refresh_token: req.session.tokenInfo.refresh_token
  }, {
    headers: {
      Accept: 'application/json',
    }
  }).then(result => {
    req.session.tokenInfo = result.data;
    res.json(req.session.tokenInfo);
  }).catch((e: AxiosError) => {
    res.json(e.response.data).status(e.response.status);
  });
};

export const authRouter = Router();

authRouter.get('/login', login);
authRouter.get('/redirect', redirectHandler);
authRouter.get('/refresh', refreshToken);
