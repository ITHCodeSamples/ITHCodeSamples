export interface SessionInterface {
  driver: 'local' | 'redis';
  redis?: {
    port: number;
    host: string;
  };
  secret: string;
  saveUninitialized: boolean;
}
