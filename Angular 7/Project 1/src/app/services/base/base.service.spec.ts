
import {TestBed} from '@angular/core/testing';

import {BaseService} from './base.service';
import {environment} from '../../environments/environment';

describe('BaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BaseService = TestBed.get(BaseService);
    expect(service).toBeTruthy();
  });

  it('should return api url', () => {
    expect(BaseService.url('sample')).toBe(`${environment.token_auth_config.apiBase}/sample`);
  });
});