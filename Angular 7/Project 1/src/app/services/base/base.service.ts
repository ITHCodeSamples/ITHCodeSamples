import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor() {
  }

  /**
   * Returns api url
   * @param url Main url action of api
   */
  public static url(url: string): string {
    return `${environment.token_auth_config.apiBase}/${url}`;
  }
}