import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from './base.service';
import {map} from 'rxjs/operators';

export interface Organization {
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(public http: HttpClient) {
  }

  getOrganizations(params: {
    search_str: string
  } | null = null): Observable<Organization[]> {
    const data = {
      params: params ? params : {}
    };
    return this.http.get(BaseService.url(`organizations`), data).pipe(map(obj => (<any>obj).organizations as Organization[]));
  }
}