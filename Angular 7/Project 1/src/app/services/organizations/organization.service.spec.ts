import {getTestBed, TestBed} from '@angular/core/testing';

import {Organization, OrganizationService} from './organization.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {BaseService} from './base.service';

describe('OrganizationService', () => {
  let httpMock: HttpTestingController;
  let injector: TestBed;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    injector = getTestBed();
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: OrganizationService = TestBed.get(OrganizationService);
    expect(service).toBeTruthy();
  });

  it('should return Observable<Organization[]> on listing', function () {
    const dummyData: Organization[] = [<Organization>{
      id: 1,
      name: 'dummy'
    }];
    const service: OrganizationService = injector.get(OrganizationService);
    service.getOrganizations().subscribe(organizations => expect(<any>organizations).toEqual(dummyData));
    const req = httpMock.expectOne(BaseService.url(`organizations`));
    expect(req.request.method).toBe('GET');
    req.flush({
      organizations: dummyData
    });
  });
});