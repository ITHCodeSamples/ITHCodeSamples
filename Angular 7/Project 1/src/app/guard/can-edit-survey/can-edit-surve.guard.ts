import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {SurveyService} from '../../services/survey.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {API_ERROR_ACTION, API_ERROR_MESSAGE, GlobalService} from '../../services/global.service';

@Injectable({
  providedIn: 'root'
})
export class CanEditSurveyGuard implements CanActivate {
  constructor(public surveyService: SurveyService, public route: Router, public spinner: NgxSpinnerService,
              public globalService: GlobalService) {
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
    try {
      this.spinner.show();
      const survey = await this.surveyService.getSurvey(next.params.survey_id).toPromise();
      if (survey.suvery_run_count_for_survey_template === null || survey.suvery_run_count_for_survey_template === 0) {
        return true;
      } else {
        this.route.navigate(['/search-surveys']).then(() => {
          this.globalService.openSnackBar('Active surveys cannot be edited', API_ERROR_ACTION);
        });
      }
    } catch (e) {
      this.route.navigate(['/search-surveys']).then(() => {
        this.globalService.openSnackBar(API_ERROR_MESSAGE, API_ERROR_ACTION);
      });
    } finally {
      this.spinner.hide();
    }
  }
}
