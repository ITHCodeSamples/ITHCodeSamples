import {TestBed, async, inject} from '@angular/core/testing';

import {CanEditSurveyGuard} from './can-edit-survey.guard';
import {ANGULAR_TOKEN_OPTIONS} from 'angular-token';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBarModule} from '@angular/material';
import {of} from 'rxjs';
import {dummySurvey} from '../../models/dummy-data';
import {ActivatedRouteSnapshot, NavigationEnd, Router, RouterStateSnapshot, Routes} from '@angular/router';
import {DashboardComponent} from '../../pages/dashboard/dashboard.component';
import {SurveyComponent} from '../../pages/survey/survey.component';
import {DashboardModule} from '../../pages/dashboard/dashboard.module';
import {SurveyModule} from '../../pages/survey/survey.module';
import {filter} from 'rxjs/operators';

const routes: Routes = [{
  path: 'search-surveys', component: DashboardComponent
},
  {path: 'survey/1/edit', component: SurveyComponent}
];

describe('CanEditSurveyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes), MatSnackBarModule, DashboardModule, SurveyModule],
      providers: [CanEditSurveyGuard, {
        provide: ANGULAR_TOKEN_OPTIONS,
        useValue: {
          apiBase: 'http://sample-base.com'
        }
      }, {
        provide: ActivatedRouteSnapshot,
        useValue: {
          params: {'survey_id': 1}
        }
      }]
    });
  });

  it('should create', inject([CanEditSurveyGuard], (guard: CanEditSurveyGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should redirect to search-surveys page when edit an active survey', inject([CanEditSurveyGuard], (guard: CanEditSurveyGuard) => {
    let route: ActivatedRouteSnapshot;
    let router: Router;
    route = TestBed.get(ActivatedRouteSnapshot);
    router = TestBed.get(Router);
    spyOn(guard.globalService, 'openSnackBar');
    spyOn(guard.surveyService, 'getSurvey').and.returnValue(of({...dummySurvey, suvery_run_count_for_survey_template: 5}));
    spyOn(guard.spinner, 'show');
    const mockSnapshot: any = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
    guard.canActivate(route, mockSnapshot);
    expect(guard.surveyService.getSurvey).toHaveBeenCalledWith(1);
    router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(event => {
      expect((<NavigationEnd>event).url).toEqual('/search-surveys');
    });
  }));
});