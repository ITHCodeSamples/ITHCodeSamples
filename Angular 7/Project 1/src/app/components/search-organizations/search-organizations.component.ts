import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, finalize, switchMap} from 'rxjs/operators';
import {Organization, OrganizationService} from '../../../services/organization.service';
import {API_ERROR_MESSAGE, GlobalService} from '../../../services/global.service';

@Component({
  selector: 'app-search-organizations',
  templateUrl: './search-organizations.component.html',
  styleUrls: ['./search-organizations.component.scss']
})
export class SearchOrganizationsComponent implements OnInit {

  @Output() selected = new EventEmitter<Organization>();
  @Input() organization = '';
  @Input() placeholder = 'Search Organization';
  public organizationChange: Subject<string> = new Subject<string>();

  public organizations: Organization[] = [];

  public searching = false;

  constructor(private organizationService: OrganizationService, private globalService: GlobalService) {
  }

  ngOnInit() {
    this.organizationChange.asObservable().pipe(debounceTime(500),
      filter(organizationValue => {
        if (organizationValue.length === 0) {
          this.organizations = [];
          this.selected.emit(null);
          return false;
        } else {
          return true;
        }
      }),
      switchMap(organizationValue => this.searchOrganization(organizationValue)))
      .subscribe(organizations => this.organizations = organizations,
        () => this.globalService.openSnackBar(API_ERROR_MESSAGE, 'Error'));
  }

  public listOrganizations($event) {
    $event.preventDefault();
    this.organizationChange.next(this.organization);
  }

  public searchOrganization(searchText) {
    this.searching = true;
    return this.organizationService.getOrganizations({search_str: searchText})
      .pipe(finalize(() => this.searching = false));
  }

  selectOrganization(organization) {
    this.selected.emit(organization);
  }
}
