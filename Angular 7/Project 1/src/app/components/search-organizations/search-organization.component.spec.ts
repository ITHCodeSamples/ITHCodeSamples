import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SearchOrganizationsComponent} from './search-organizations.component';
import {SearchOrganizationsModule} from './search-organizations.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatSnackBarModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('SearchOrganizationsComponent', () => {
  let component: SearchOrganizationsComponent;
  let fixture: ComponentFixture<SearchOrganizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SearchOrganizationsModule, HttpClientTestingModule, MatSnackBarModule, NoopAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchOrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have input button', function () {
    expect(fixture.nativeElement.querySelector('.search-organization input')).toBeTruthy();
  });
});